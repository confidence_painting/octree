#include <cuda.h>
#include <iostream>
#include <curand.h>
#include <curand_kernel.h>

#include "octree/octree.h"
#include "octree/cuda_raycaster.h"
#include "octree/raycast.cu"

#define checkCudaErrors(val) check_cuda( (val), #val, __FILE__, __LINE__ )

namespace octree {

void check_cuda(cudaError_t result, char const *const func, const char *const file, int const line) {
    if (result) {
        std::cerr << "CUDA error = " << static_cast<unsigned int>(result) << " at " <<
            file << ":" << line << " '" << func << "' \n";
        // Make sure we call CUDA Device Reset before exiting
        cudaDeviceReset();
        exit(99);
    }
}

__global__ void
init_randoms(
    unsigned int seed,
    curandState_t* states,
    int state_width)
{
  int rand_ind = threadIdx.x + threadIdx.y * state_width;
  curand_init(seed, rand_ind, 0, &states[rand_ind]);
}

__global__ void
score_count_projection_kernel(
    CameraConfig * cfg,
    OctreeConfig * tree_cfg,
    long * octree_data,
    float * score,
    float * total_counts,
    curandState_t* states,
    int state_width)
{
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    float dist = 0;
    float3 pos, ray;
    int pixel_index = i*cfg->im_cols + j;
    int rand_ind = threadIdx.x + threadIdx.y * state_width;
    cudatree::calcRayPos(cfg,
        i+curand_uniform(&states[rand_ind])-0.5,
        j+curand_uniform(&states[rand_ind])-0.5,
        &pos, &ray);
    long hit = cudatree::raycast(tree_cfg, pos, ray, &dist, octree_data);

    float * float_data = (float *) octree_data;
    if (hit != cudatree::NULL_VOX) {
      atomicAdd(&score[pixel_index], float_data[2*hit]);
      atomicAdd(&total_counts[pixel_index], float_data[2*hit+1]);
      /*
      // Perform voxel size calculations to check if additional voxels need to be checked
      float y = (i - cfg->cy)/cfg->fy;
      float x = (j - cfg->cx)/cfg->fx;
      float dist_to_plane = sqrt(x*x + y*y + 1);
      float depth = dist/dist_to_plane;

      // Perform voxel size calculations to check if additional voxels need to be checked
      float pixel_size = depth / min(cfg->im_rows, cfg->im_rows);
      float pixels_per_voxel = tree_cfg->res / pixel_size;
      if (pixels_per_voxel < -0.5) {
        for (float sub_i=0; sub_i<1; sub_i+=pixels_per_voxel) {
          for (float sub_j=0; sub_j<1; sub_j+=pixels_per_voxel) {
            cudatree::calcRayPos(cfg, i+sub_i, j+sub_j, &pos, &ray);
            hit = cudatree::raycast(tree_cfg, pos, ray, &dist, octree_data);
            if (hit != cudatree::NULL_VOX) {
              atomicAdd(&score[pixel_index], float_data[2*hit]);
              atomicAdd(&total_counts[pixel_index], float_data[2*hit+1]);
            }
          }
        }
      } else {
        atomicAdd(&score[pixel_index], float_data[2*hit]);
        atomicAdd(&total_counts[pixel_index], float_data[2*hit+1]);
      }
      */
    }
}

__global__ void
map_projection_kernel(
    CameraConfig * cfg,
    OctreeConfig * tree_cfg,
    long * octree_data,
    float * score,
    curandState_t* states,
    int state_width)
{
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    float dist = 0;
    float3 pos, ray;
    int pixel_index = i*cfg->im_cols + j;
    int rand_ind = threadIdx.x + threadIdx.y * state_width;
    cudatree::calcRayPos(cfg,
        i+curand_uniform(&states[rand_ind])-0.5,
        j+curand_uniform(&states[rand_ind])-0.5,
        &pos, &ray);
    long hit = cudatree::raycast(tree_cfg, pos, ray, &dist, octree_data);
    // Hit is the index of the node
    float * float_data = (float *) octree_data;

  
    if (hit != cudatree::NULL_VOX) {
      atomicAdd(&float_data[2*hit], score[pixel_index]);
      atomicAdd(&float_data[2*hit+1], 1.0f);
      /*
      // Convert dist to depth
      float y = (i - cfg->cy)/cfg->fy;
      float x = (j - cfg->cx)/cfg->fx;
      float dist_to_plane = sqrt(x*x + y*y + 1);
      float depth = dist/dist_to_plane;

      // Perform voxel size calculations to check if additional voxels need to be checked
      float pixel_size = depth / cfg->im_rows;
      float pixels_per_voxel = tree_cfg->res / pixel_size;
      // selective upsampling
      if (pixels_per_voxel < -0.5) {
        for (float sub_i=0; sub_i<1; sub_i+=pixels_per_voxel) {
          for (float sub_j=0; sub_j<1; sub_j+=pixels_per_voxel) {
            cudatree::calcRayPos(cfg, i+sub_i, j+sub_j, &pos, &ray);
            hit = cudatree::raycast(tree_cfg, pos, ray, &dist, octree_data);
            if (hit != cudatree::NULL_VOX) {
              atomicAdd(&float_data[2*hit], score[pixel_index]);
              atomicAdd(&float_data[2*hit+1], 1.0f);
            }
          }
        }
      } else {
        atomicAdd(&float_data[2*hit], score[pixel_index]);
        atomicAdd(&float_data[2*hit+1], 1.0f);
      }
      */
    }
}

__global__ void
depth_projection_kernel(
    CameraConfig * cfg,
    OctreeConfig * tree_cfg,
    long * octree_data,
    float * depth,
    curandState_t* states,
    int state_width)
{
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    float dist = 0;
    float3 pos, ray;
    int pixel_index = i*cfg->im_cols + j;
    cudatree::calcRayPos(cfg, i, j, &pos, &ray);
    cudatree::raycast(tree_cfg, pos, ray, &dist, octree_data);

    depth[pixel_index] = dist;
}

CudaRaycaster::CudaRaycaster(octree::Octree & tree, CameraConfig & cfg) {
  assert(tree.current_max_depth <= cudatree::MAX_DEPTH);
  tx = 8;
  ty = 8;
  size_t size = tree.numNodes()*8;
  int64_t * h_octree_data = new int64_t[size];
  for (size_t i=0; i<tree.numNodes(); i++) {
    for (size_t j=0; j<8; j++) {
      h_octree_data[i*8+j] = tree.getNode(i)->children[j];
    }
  }
  printf("Tree size: %i\n", size);
  this->cfg = cfg;

  OctreeConfig h_cfg = {
    .max_depth=tree.current_max_depth, 
    .res = tree.scale,
    .origin = {tree.origin.x, tree.origin.y, tree.origin.z}
  };

  // state_width = cfg.im_cols/ty;
  state_width = tx;
  // int height = cfg.im_cols/ty;
  checkCudaErrors(cudaMalloc(&states, tx*ty*sizeof(curandState_t)));
  dim3 blocks (cfg.im_rows/tx, cfg.im_cols/ty);
  dim3 threads (tx, ty);
  init_randoms<<<blocks, threads>>>(time(0), states, state_width);

  checkCudaErrors(cudaMalloc(&octree_data, size*sizeof(int64_t)));
  checkCudaErrors(cudaMalloc(&tree_cfg, sizeof(OctreeConfig)));
  checkCudaErrors(cudaMalloc(&ccfg, sizeof(CameraConfig)));

  checkCudaErrors(cudaMalloc(&cdepth, cfg.im_cols*cfg.im_rows*sizeof(float)));
  checkCudaErrors(cudaMalloc(&cscore, cfg.im_cols*cfg.im_rows*sizeof(float)));
  checkCudaErrors(cudaMalloc(&ccount, cfg.im_cols*cfg.im_rows*sizeof(float)));

  checkCudaErrors(cudaMemcpy(tree_cfg, &h_cfg, sizeof(OctreeConfig), cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(octree_data, h_octree_data, size*sizeof(int64_t), cudaMemcpyHostToDevice));
  delete[] h_octree_data;
}

CudaRaycaster::~CudaRaycaster() {
  checkCudaErrors(cudaFree(octree_data));
  checkCudaErrors(cudaFree(tree_cfg));
  checkCudaErrors(cudaFree(ccfg));
  checkCudaErrors(cudaFree(cdepth));
  checkCudaErrors(cudaFree(cscore));
  checkCudaErrors(cudaFree(ccount));
  checkCudaErrors(cudaFree(states));
}

void CudaRaycaster::renderDepth(CameraConfig & cfg, float * image)
{
  assert(this->cfg.im_rows == cfg.im_rows);
  assert(this->cfg.im_cols == cfg.im_cols);
  dim3 blocks (cfg.im_rows/tx, cfg.im_cols/ty);
  dim3 threads (tx, ty);
  checkCudaErrors(cudaMemcpy(ccfg, &cfg, sizeof(CameraConfig), cudaMemcpyHostToDevice));
  depth_projection_kernel<<<blocks, threads>>>(
    ccfg,
    tree_cfg,
    octree_data,
    cdepth,
    states, state_width);
  checkCudaErrors(cudaGetLastError());
  checkCudaErrors(cudaDeviceSynchronize());
  checkCudaErrors(cudaMemcpy(image, cdepth, cfg.im_cols*cfg.im_rows*sizeof(float), cudaMemcpyDeviceToHost));
}

void CudaRaycaster::renderScoreCount(CameraConfig & cfg, float * score, float * count, int n)
{
  assert(this->cfg.im_rows == cfg.im_rows);
  assert(this->cfg.im_cols == cfg.im_cols);
  dim3 blocks (cfg.im_rows/tx, cfg.im_cols/ty);
  dim3 threads (tx, ty);
  size_t im_size = cfg.im_cols*cfg.im_rows*sizeof(float);
  checkCudaErrors(cudaMemcpy(ccfg, &cfg, sizeof(CameraConfig), cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemset(cscore, 0, im_size));
  checkCudaErrors(cudaMemset(ccount, 0, im_size));
  for (int i=0; i<n; i++) {
    score_count_projection_kernel<<<blocks, threads>>>(
      ccfg,
      tree_cfg,
      octree_data,
      cscore, ccount,
      states, state_width);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
  }
  checkCudaErrors(cudaMemcpy(score, cscore, im_size, cudaMemcpyDeviceToHost));
  checkCudaErrors(cudaMemcpy(count, ccount, im_size, cudaMemcpyDeviceToHost));
}

void CudaRaycaster::projectScore(CameraConfig & cfg, float * score, int n)
{
  assert(this->cfg.im_rows == cfg.im_rows);
  assert(this->cfg.im_cols == cfg.im_cols);
  dim3 blocks (cfg.im_rows/tx, cfg.im_cols/ty);
  dim3 threads (tx, ty);

  checkCudaErrors(cudaMemcpy(ccfg, &cfg, sizeof(CameraConfig), cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(cscore, score, cfg.im_cols*cfg.im_rows*sizeof(float), cudaMemcpyHostToDevice));
  for (int i=0; i<n; i++) {
    map_projection_kernel<<<blocks, threads>>>(
      ccfg,
      tree_cfg,
      octree_data,
      cscore,
      states, state_width);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
  }
}

};
