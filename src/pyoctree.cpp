#include <torch/extension.h>

#include <vector>
#include <iostream>
#include <fstream>
#include <cuda.h>

#define LOG() printf("%s: %i\n", __FILE__, __LINE__)

#define CHECK_CUDA(x) TORCH_CHECK(x.type().is_cuda(), #x " must be a CUDA tensor")

std::vector<torch::Tensor>
cuda_reproject_score_maps(
    std::string octomap_path,
    std::vector<torch::Tensor> & score_maps,
    std::vector<torch::Tensor> & pose_tensors,
    std::vector<float> & calib, 
    int n);

std::vector<torch::Tensor>
reproject_score_maps(
    std::vector<torch::Tensor> score_maps,
    std::vector<torch::Tensor> pose_tensors,
    std::string & octomap_path,
    std::vector<float> & calib,
    int n)
{

  /*!
   * A variation of the SuperPoint algorithm applied to bounding boxes
   *
   * pose_paths: list of paths to poses for each image in the batch
   * calib: [fx, fy, cx, cy]
   **/

  // ----------------------------- LOADING ---------------------------------- //

  // -------------------------- DONE LOADING -------------------------------- //

  /*
  std::vector<torch::Tensor> targets = cuda_reproject_score_maps(
      tree_data,
      float(tree.scale),
      tree.current_max_depth,
      {float(tree.origin.x), float(tree.origin.x), float(tree.origin.x)},
      score_maps,
      pose_tensors,
      calib);
  */

  std::vector<torch::Tensor> targets = cuda_reproject_score_maps(
      octomap_path, score_maps, pose_tensors, calib, n);

  return targets;
}

PYBIND11_MODULE(TORCH_EXTENSION_NAME, m) {
  m.def("reproject_score_maps", &reproject_score_maps, py::arg("score_maps"), py::arg("pose_tensors"), py::arg("octomap_path"), py::arg("calib"), py::arg("n") = 5);
}

