from setuptools import setup
from torch.utils.cpp_extension import BuildExtension, CUDAExtension
import os
from pathlib import Path

current_dir = Path(__file__).parent.absolute()

setup(
    name='pyoctree',
    ext_modules=[
        CUDAExtension(
            'pyoctree',
            sources=[ 'src/pyoctree.cpp', 'src/pyoctree_cuda.cu', 'src/octree.cpp'],
            include_dirs=[os.path.join(current_dir, 'include')],
            #  extra_compile_args={'cxx': ['-g'], 'nvcc': ['-O2']},
            extra_compile_args={'cxx': [], 'nvcc': ['-O2']},
        )
    ],
    cmdclass={
        'build_ext': BuildExtension
    })
