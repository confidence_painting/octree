#pragma once
#include "octree/octree.h"
#include <cuda_runtime.h>
#include <curand.h>
#include <curand_kernel.h>

namespace octree {

typedef struct CameraConfig {
  int im_rows, im_cols;
  float fx, fy, cx, cy;
  float transform_matrix[16];
} CameraConfig;

typedef struct OctreeConfig {
  int max_depth;
  float res;
  float3 origin;
} OctreeConfig;

class CudaRaycaster {
  public:
  long * octree_data;
  OctreeConfig * tree_cfg;
  CameraConfig cfg;
  CameraConfig * ccfg;
  curandState_t * states;
  int tx, ty, state_width;

  float *ccount, *cscore, *cdepth;

  CudaRaycaster(octree::Octree & tree, CameraConfig & cfg);
  CudaRaycaster() {};
  ~CudaRaycaster();
  void renderDepth(CameraConfig & cfg, float * image);
  void projectScore(CameraConfig & cfg, float * score, int n);
  void renderScoreCount(CameraConfig & cfg, float * score, float * count, int n);
};
};
