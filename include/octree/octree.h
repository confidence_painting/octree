// MIT License
//
// Copyright (c) 2020 Alexander Mai
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once
#include <stdint.h>
#include <string>
#include <vector>
#include <limits>
#include "glm/glm.hpp"

// typedef struct vec3 { float x, y, z; } vec3;

namespace octree {

typedef struct OctreeNode {
  int64_t children[8];
} OctreeNode;

typedef struct FLinkedList {
  float score;
  int address;
  int child;
  FLinkedList(float score, int address, int child) : score(score), address(address), child(child) {};
  FLinkedList() : score(0), address(-1), child(-1) {};
} FLinkedList;

OctreeNode emptyNode();

using glm::vec3;

// Here is how the voxels are numbered
const vec3 VOXEL_NUMBERING[8] = {
  { 1,  1,  1},
  { 1,  1, -1},
  { 1, -1,  1},
  { 1, -1, -1},
  {-1,  1,  1},
  {-1,  1, -1},
  {-1, -1,  1},
  {-1, -1, -1}
};

const int64_t NULL_VOX = -1;
const float EPS = 1e-6;
const size_t NUM_EXTRA_FIELDS = 5;
// const size_t MAX_CHUNK_SIZE = 2e9;// 4GB
// const size_t MAX_CHUNK_SIZE = 2e6;// 4GB
const size_t HEADER_SIZE = NUM_EXTRA_FIELDS*sizeof(int64_t);

const size_t NUM_DATA_PER_NODE = 8;

bool rayBoxIntersect ( vec3 rpos, vec3 rdir, vec3 vmin, vec3 vmax, float * ray_length);
void vec3print(vec3 a);

int
sort_insert(
    FLinkedList * list,
    const int head_index,
    const int desired_address,
    const float score);

class Octree {
  public:
    float scale;
    vec3 origin;
    int current_max_depth;

    // init and save functions
    Octree(float scale, vec3 origin={0, 0, 0});
    Octree(std::string fname, size_t max_chunk_size=2e9);
    Octree() : scale(0) {};
    int save(std::string fname, size_t max_chunk_size=2e9);

    // Simple access functions
    bool withinBounds(vec3 pt);
    void incrementDepth();
    float getRadius();
    int getDepth() {return current_max_depth;}
    // These functions are abstracted for easy porting to the GPU
    OctreeNode * getNode(int64_t index) {return &data[index];};
    size_t numNodes() {return data.size();};

    // Index generation functions
    float 
    voxelCoord(float v, int depth);
    int childIndex(vec3 pt, int depth);

    // Main functions for use
    int64_t * get(vec3 pt);
    void insert(vec3 pt, int64_t data);
    int64_t * raycast(const vec3 & source, const vec3 & dir, float * dist);

    // Reading functions
    void binarize(char * buffer);
    int initFromBinary(char * buffer, size_t length);
    size_t getBinarySize();


  protected:
    std::vector<OctreeNode> data;
};
};
