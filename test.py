import torch
import pyoctree
import matplotlib.pyplot as plt
import numpy as np

#  octree_path = "octrees/RealisticRendering.oct"
#  octree_path = "/data/unrealcv/octomaps/ArchinteriorsVol2Scene1.oct"
octree_path = "/data/unrealcv/octomaps/ArchinteriorsVol2Scene2.oct"
#  h = w = 120
h = w = 480
#  h = w = 1080

calib = [w/2, h/2, w/2, h/2]

N = 10
score_maps = [torch.ones(h, w).float() for i in range(N)]
pose_tensors = []
for i in range(N):
    pose = torch.eye(4).float()
    pose[:3, 3] = torch.tensor([0, -1.1, -2.15])
    pose[:3, 3] += torch.rand(3)*0.2
    pose_tensors.append(pose)

for i in range(1):
    out = pyoctree.reproject_score_maps(score_maps, pose_tensors, octree_path, calib, 3)
scores = out[:N]
counts = out[N:]
for i, (s, c) in enumerate(zip(scores, counts)):
    #  plt.imshow(c.cpu())
    #  plt.show()
    fig, axs = plt.subplots(2)
    axs[0].imshow(s.cpu())
    axs[1].imshow(c.cpu())
    plt.show()
