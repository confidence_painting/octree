/// PATH MARCHING
vec4 light_sphere;


vec3 light_distr(vec3 p)
{
    return vec3(1,1,1) * (4.*step(-light_sphere.w, -length(p - light_sphere.xyz)));
}

vec3 sky(vec3 ray)
{
    return 0.2*(cos(0.001*iTime)+1.)*vec3(1.15,1.1,1.0)*sqrt(1.-0.95*ray.z*ray.z)*(tanh(10.*ray.z)+1.);
}


vec3 path_march(vec3 p, vec3 ray, float t, float i, float angle, float seed)
{
    vec3 fincol = vec3(1.), finill = vec3(0.);
    vec4 res = vec4(0.);
    for(float b = 0.; (b < MAX_BOUNCE); b++)
    {
        if(b < 1.)
        {
            float h = map(p).w;
            if (h < angle*t || t > MAX_DIST)
            {
                 res = vec4(p, h);
            }
        }
       
        if(res.xyz != p)
        {
            //march next ray
       		res = trace(p, ray, t, i, angle);
        }
         
        if(t > MAX_DIST || (i >= MAX_STEPS && res.w > 5.*angle*t))
        {
            finill += sky(ray)*fincol;
            break;
        }
        
        /// Surface interaction
        vec3 norm = calcNormal(res.xyz, res.w);    
        //discontinuity correction
        vec3 surfpt = res.xyz - (res.w - 2.*angle*t)*norm;
        
        vec3 refl = reflect(ray, norm);
        
        float refl_prob = hash(seed*SQRT2);
       
        //random diffusion, random distr already samples cos(theta) closely
        if(refl_prob < reflection)
        {
            vec3 rand = clamp(pow(1.-reflection,4.)*randn(seed*SQRT3),-1.,1.);
            ray = normalize(refl + rand);
        }
        else
        {
            vec3 rand = random_sphere(seed*SQRT3);
            ray = normalize(norm + rand);
        }
      

        //color and illuminaition
        /* vec3 colp = map(p).xyz; */
        vec3 colp = map(surfpt).xyz;
        fincol = fincol*clamp(colp.xyz,0.,1.);
        
        //add fractal glow
        finill += 5.*light_distr(surfpt)*fincol;

        vec3 fadept = vec3(0.2,
                           0.3+0.01*cos(iTime),
                           0.75+0.01*sin(iTime));

        float clamped_dist = clamp(pow(length(colp.xyz-fadept), 2.),0.,1.);
        finill += vec3(5.)*exp(-300.*clamped_dist)*fincol;

        finill += vec3(0.6)*exp(-300.*clamp(pow(abs(length(colp.xyz-vec3(0.3,0.8,0.3))),2.),0.,1.))*fincol;
        
        angle *= 1.15;
    }
    
    return finill;
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
     // Normalized centered pixel coordinates 
    vec2 pos = (fragCoord - iResolution.xy*0.5)/max(iResolution.x,iResolution.y);
    
    vec2 angles = texelFetch(iChannel3,  ivec2(ANGLE_INDX,0), 0).xy;
    
    vec3 rand = 2.*blue3(2.*fragCoord, iTime) - 1.;
    vec3 ray = getRay(angles, pos+0.5*rand.xy/iResolution.x);
    vec4 p = texelFetch(iChannel3,  ivec2(POS_INDX,0), 0);
    
    light_sphere = texelFetch(iChannel3,  ivec2(LIGHT_INDX,0), 0);

    /* mat3 camera = transpose(getCamera(angles.xy)); */
    /* light_sphere = texelFetch(iChannel3,  ivec2(POS_INDX,0), 0); */
    /* light_sphere.xyz += camera[1]*1.0; */

    light_sphere.xyz += 0.*light_sphere.w*vec3(sin(iTime), cos(iTime), 0.);
    
    /*
    fragColor = vec4(0.);
    float iter = 0., td = 0.;
    vec4 res = trace(p.xyz, ray, td, iter, LOD);
    for(float i = 0.; i <SPP; i++)
    {
        fragColor.xyz += path_march(res.xyz, ray, td, iter, LOD*2., rand.x+hash(iTime+i));
    }
    */
    float dist;
    int64_t result = raycast(p.xyz, ray, dist);
    fragColor.xyz = getColorFromNode(result);
    
    fragColor.xyz /= SPP;

    vec4 posit = texelFetch(iChannel3,  ivec2(POS_INDX,0), 0);
   
    vec4 prev = texture(iChannel0, fragCoord/iResolution.xy);

    float avg = (posit.w>1.)?AVG*tanh(posit.w*0.5*sqrt(1.-AVG)):0.25;
    fragColor.xyz = avg*prev.xyz + (1.- avg)*fragColor.xyz;
    fragColor.w = posit.w;
    if(iFrame < 1)
    {
        fragColor = vec4(0.);
    }
}
