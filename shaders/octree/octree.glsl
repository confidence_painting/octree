uniform float scale = 0.01;
uniform vec3 origin = vec3(0, 0, 0);
// const uniform int current_max_depth = 3;
#define current_max_depth 3
#define max_len int(pow(2, current_max_depth))
// const int max_len = int(pow(2, current_max_depth));

const float EPS = 1e-6;
const int64_t NULL_VOX = -1;

// Here is how the voxels are numbered
const vec3 VOXEL_NUMBERING[8] = {
  { 1,  1,  1},
  { 1,  1, -1},
  { 1, -1,  1},
  { 1, -1, -1},
  {-1,  1,  1},
  {-1,  1, -1},
  {-1, -1,  1},
  {-1, -1, -1}
};


struct FLinkedList {
  float score;
  int address;
  int child;
};

FLinkedList list[8];
// initialize stack
vec3 voxel_centers[max_len];
int depths[max_len];

vec3 getColorFromNode(int64_t data) {
  int64_t r = data & 255;
  int64_t g = (data >> 3) & 255;
  int64_t b = (data >> 6) & 255;
  return vec3(float(r)/255., float(g)/255., float(b)/255.);
}

bool
rayBoxIntersect ( vec3 rpos, vec3 rdir, vec3 vmin, vec3 vmax, out float ray_length)
{
  // posted by zacharmarz
  // https://gamedev.stackexchange.com/questions/18436/most-efficient-aabb-vs-ray-collision-algorithms

  // rdir is unit direction vector of ray
  vec3 dirfrac = vec3(1/rdir.x, 1/rdir.y, 1/rdir.z);
  // rpos is origin of ray
  float t1 = (vmin.x - rpos.x)*dirfrac.x;
  float t2 = (vmax.x - rpos.x)*dirfrac.x;
  float t3 = (vmin.y - rpos.y)*dirfrac.y;
  float t4 = (vmax.y - rpos.y)*dirfrac.y;
  float t5 = (vmin.z - rpos.z)*dirfrac.z;
  float t6 = (vmax.z - rpos.z)*dirfrac.z;

  float tmin = max(max(min(t1, t2), min(t3, t4)), min(t5, t6));
  float tmax = min(min(max(t1, t2), max(t3, t4)), max(t5, t6));

  // if tmax < 0, ray (line) is intersecting AABB, but the whole AABB is behind us
  if (tmax < 0)
  {
      ray_length = tmax;
      return false;
  }

  // if tmin > tmax, ray doesn't intersect AABB
  if (tmin > tmax)
  {
      ray_length = tmax;
      return false;
  }

  ray_length = tmin;
  return true;
}

int
sort_insert(
    const int head_index,
    const int desired_address,
    const float score)
{
  // Returns the new head index

  FLinkedList item = FLinkedList(score, desired_address, -1);

  int new_head_index = head_index;
  int selection = head_index;
  int parent = head_index;
  // Traverse linked list and sort
  if (head_index == -1) {
    // init linked list
    item.child = -1;
    list[0] = item;
    return 0;
  } else {
    while (selection != -1 && list[selection].score < item.score) {
      parent = selection;
      selection = list[selection].child;
    }
    if (selection == parent) {
      if (list[selection].score > item.score) {
        item = FLinkedList(item.score, desired_address, head_index);
        new_head_index = item.address;
      } else {
        item = FLinkedList(score, desired_address, -1);
        list[parent].child = desired_address;
      }
    } else {
      item = FLinkedList(item.score, desired_address, selection);
      list[parent].child = desired_address;
    }
  }
  item.address = desired_address;
  list[item.address] = item;
  return new_head_index;
}

int64_t
getNodeChild(int64_t index, int child) {
  return NULL_VOX;
}


int64_t
raytrace_recurs(const vec3 source,
                const vec3 ray,
                const vec3 voxel_origin,
                const int64_t voxel_ind,
                const int depth,
                out float dist)
{
  // The first thing we need to do is establish a traversal order for the voxels
  // Sort voxels by distance in increasing order
  vec3 centers[8];
  int head = -1;
  int selection = -1;
  float s = scale*pow(2, current_max_depth-depth);
  for (int i=0; i<8; i++) {
    if (getNodeChild(voxel_ind, i) == NULL_VOX) {
      continue;
    }
    const vec3 bound = VOXEL_NUMBERING[i];

    centers[i] = voxel_origin + s*bound/2.0f;
    vec3 vmin = centers[i] - s/2.0f;
    vec3 vmax = centers[i] + s/2.0f;

    float ray_length;
    bool collision = rayBoxIntersect(source, ray, vmin, vmax, ray_length);

    if (!collision) {
      continue;
    }
    head = sort_insert(head, i, ray_length);
  }
  selection = head;

  // Iterate through the sorted voxels
  while (selection != -1) {
    // Update index
    int j = list[selection].address;
    float d = list[selection].score;
    selection = list[selection].child;
    int64_t new_ind = getNodeChild(voxel_ind, j);

    // Check if voxel is empty
    if (new_ind == NULL_VOX) {
      continue;
    }

    // If this is the maximum depth, return
    if (depth == current_max_depth) {
      dist = d;
      return new_ind;
    }
    // else, dive deeper into tree
    int64_t res = raytrace_recurs(source, ray, centers[j], new_ind, depth + 1, dist);
    if (res != NULL_VOX) {
      return res;
    } else {
      continue;
    }
  }
  return NULL_VOX;
}

int64_t
raycast(const vec3 source,
        const vec3 dir,
        out float  dist)
{
  // The first thing we need to do is establish a traversal order for the voxels
  // Sort voxels by distance in increasing order

  // Initialize list with origin and depth 0 key
  // keys are terminated with -1

  // We have a rotating stack
  // Take the head
  // Iterate through the 8 children and sort them by distance
  // put new octrees on top of stack in order of distance
  // loop
  vec3 ray = normalize(dir);

  // Initialize the depths so we know when we run out
  for (int i=0; i<max_len; i++) {
    depths[i] = -1;
  }
  int64_t voxel_inds[max_len];

  int stack_head = 0;
  voxel_centers[stack_head] = origin;
  depths[stack_head] = 0;
  voxel_inds[stack_head] = 0;

  while (depths[stack_head] != -1) {
    vec3 voxel_origin = voxel_centers[stack_head];
    int64_t voxel_ind = voxel_inds[stack_head];
    int depth = depths[stack_head];

    int head = -1;
    float s = scale*pow(2, current_max_depth-depth);
    FLinkedList list[8];
    vec3 centers[8];
    for (int i=0; i<8; i++) {
      if (getNodeChild(voxel_ind, i) == NULL_VOX) {
        continue;
      }
      vec3 bound = VOXEL_NUMBERING[i];

      centers[i] = voxel_origin + s*bound/2.0f;
      vec3 vmin = centers[i] - s/2.0f-EPS;
      vec3 vmax = centers[i] + s/2.0f+EPS;

      float ray_length;
      bool collision = rayBoxIntersect(source, ray, vmin, vmax, ray_length);

      if (!collision) {
        continue;
      }
      // sort backwards so that when we append them on to the top of the stack they are in the right order
      head = sort_insert(head, i, -ray_length);
    }
    int selection = head;

    // Iterate through the sorted voxels
    while (selection != -1) {
      // Update index
      int j = list[selection].address;
      float d = -list[selection].score;
      selection = list[selection].child;
      // int64_t new_ind = getNodeChild(voxel_ind).children[j];
      int64_t new_ind = getNodeChild(voxel_ind, j);

      // Check if voxel is empty
      if (new_ind == NULL_VOX) {
        continue;
      }

      // If this is the maximum depth, return
      if (depth == current_max_depth) {
        dist = d;
        return new_ind;
      }
      // Push onto the top of the stack
      // set stack at head to have the new variables:
      voxel_centers[stack_head] = centers[j];
      voxel_inds[stack_head] = new_ind;
      depths[stack_head] = depth+1;
      stack_head = (stack_head-1 + max_len) % max_len;
    }
    stack_head = (stack_head+1)%max_len;
  }
  return NULL_VOX;
}
