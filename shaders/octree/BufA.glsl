/// UTILITY
///
/// Using the GPU as the CPU here, pretty inefficient I guess
bool isKeyPressed(int KEY)
{
    return texelFetch( iChannel3, ivec2(KEY,0), 0 ).x > 0.5;
}
bool isKeyToggle(int KEY)
{
    return texelFetch( iChannel3, ivec2(KEY,2), 0 ).x > 0.5;
}
void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    if(iFrame < 1)
    {
        fragColor = vec4(0.);
    }
    
    if(fragCoord.x <= N && fragCoord.y <= 1.)
    {
        //instruction
        int I = int(fragCoord.x); 
        fragColor = texelFetch(iChannel0,  ivec2(I,0), 0);
        vec4 mouse = texelFetch(iChannel0,  ivec2(MOUSE_INDX,0), 0);
        vec2 mousespeed = texelFetch(iChannel0,  ivec2(MOUSE_INDX,0), 0).xy;
        vec4 angles = texelFetch(iChannel0,  ivec2(ANGLE_INDX,0), 0);
        mat3 camera = transpose(getCamera(angles.xy));
        vec4 posit = texelFetch(iChannel0,  ivec2(POS_INDX,0), 0);
        vec4 vel = texelFetch(iChannel0,  ivec2(VEL_INDX,0), 0);
        vec4 speed = texelFetch(iChannel0,  ivec2(SPEED_INDX,0), 0);
        //vec4 norm = calcGrad(posit.xyz);
       // vec4 norm_feet = calcGrad(posit.xyz-vec3(0,0,.1));
        //norm.xyz = norm.xyz/(length(norm.xyz) + 0.0001);
       // norm_feet.xyz = norm_feet.xyz/(length(norm_feet.xyz) + 0.0001);
        switch(I)
        {
        case MOUSE_INDX:  //mouse speed calculation 
            if(length(iMouse.zw - iMouse.xy) > 10.)
            {
                fragColor.xy = iMouse.xy - fragColor.zw; // mouse delta
                if(iFrame < 1)
                {
                    fragColor.xy = vec2(0.);
                }
            }
            else
            {
                fragColor.xy = vec2(0.); // mouse delta
            }
            fragColor.zw = iMouse.xy; // mouse pos
            break;
            
        case ANGLE_INDX:  //angle computation
           
            fragColor.xy = fragColor.xy + fragColor.zw*MOUSE_SENSITIVITY; // angle delta
            fragColor.y = clamp(fragColor.y, -PI*0.5, PI*0.5);
            fragColor.zw += vec2(1,-1)*mouse.xy; // mouse pos
            fragColor.zw *= 0.8;
             if(iFrame < 1)
            {
                fragColor.xy = vec2(PI*1.25,0.);
            }
            break;
            
        case POS_INDX:  //position
          
            /*float DX = length(vel.xyz*speed.x)+0.0001;
            float MAXDX = map(fragColor.xyz + vel.xyz*speed.x).w + norm.w;
            if(DX > MAXDX)
                vel *= 0.25;*/
            fragColor.xyz += vel.xyz;
            fragColor.w = vel.w;
            if(iFrame < 1)
            {
                fragColor.xyz = vec3(13.,1.,10.4);
            }
           
            break;
         case VEL_INDX:  //velocity
          
            fragColor.w++;
            if(length(mousespeed) >0. || isKeyPressed(KEY_Z))
            {
                fragColor.w = 0.;
            }
            if(isKeyPressed(KEY_UP) || isKeyPressed(KEY_W))
            {
                fragColor.xyz += camera[1]*speed.x;
                //fragColor.w = 0.;
            }
            if(isKeyPressed(KEY_DOWN) || isKeyPressed(KEY_S))
            {
                fragColor.xyz -= camera[1]*speed.x;
                //fragColor.w = 0.;
            }
            if(isKeyPressed(KEY_RIGHT) || isKeyPressed(KEY_D))
            {
                fragColor.xyz += camera[0]*speed.x;
                //fragColor.w = 0.;
            }
            if(isKeyPressed(KEY_LEFT) || isKeyPressed(KEY_A))
            {
                fragColor.xyz -= camera[0]*speed.x;
                //fragColor.w = 0.;
            }

            if(isKeyPressed(KEY_SPACE))
            {
              if (map(posit.xyz).w <= 0.05) {
                fragColor.z += 0.05;
              }
            }

            fragColor.xyz *= 0.8; //slowing down
            fragColor.z-=.002;
            
            //fractal collision detection, removing the normal velocity component 
            //fragColor.xyz += 0.*norm.xyz*max(dot(fragColor.xyz, -norm.xyz),0.)*exp(-max(norm.w,0.)/0.04);
            //               + norm_feet.xyz*max(dot(fragColor.xyz, -norm_feet.xyz),0.)*exp(-max(norm_feet.w,0.)/0.04);
            //fragColor.xyz += norm_feet.xyz*max(dot(fragColor.xyz, -norm_feet.xyz),0.)*exp(-max(norm_feet.w,0.)/0.04);
            
            //float vel_to_wall = min(dot(fragColor.xyz,norm.xyz),0.);
            //float dist_to_wall = abs(map(posit.xyz)); // not really to wall in front, just any wall
            //fragColor.xyz -= norm.xyz*vel_to_wall*(.1-dist_to_wall);
            vec3 nearest = fragColor.xyz;
            for (int i=0; i<10; i++) {
              nearest -= normalize(calcGrad(nearest+posit.xyz).xyz)*map(nearest+posit.xyz).w;
            }
            if (isnan(nearest.x+nearest.y+nearest.z)) break;
            //vec3 grad = calcGradSm(posit.xyz).xyz;
            vec3 grad = -normalize(nearest);
            float size = .05;
            
            vec3 velocity = fragColor.xyz;
            vec3 vel_normal = grad*dot(grad,fragColor.xyz)/dot(grad,grad);
            vec3 vel_tangent = fragColor.xyz - vel_normal;
            //vec3 grad2 = calcGradSm(posit.xyz+vel_tangent).xyz;
            
            float collision = (1.-exp(-1600.*length(velocity)*max(0.,1.-length(nearest-velocity)/size)));
            fragColor.xyz += (-vel_normal+.002*grad)*collision;
            
            //fragColor.xyz = vel_normal+vel_tangent;
            //fragColor.w = 0.;
            
            if (isKeyToggle(KEY_B)) fragColor.xyz = vec3(0.);
            if (length(fragColor.xyz)>.001) fragColor.w = 0.;
            
            break;
          case LIGHT_INDX:  //light
            if(isKeyPressed(KEY_L))
            {
                fragColor.xyz = posit.xyz;
                fragColor.w = .08;
            }
            if(iFrame < 1)
            {
                fragColor = vec4(12.5,-4,10.5, 0.1);
            }
            break; 
          case SPEED_INDX: //camera max speed
            if(isKeyPressed(KEY_Q))
            {
                fragColor.x *= 1.01;
            }
            if(isKeyPressed(KEY_E))
            {
                fragColor.x *= 0.99;
            }
            if(iFrame < 1)
            {
                fragColor.x = .1*CAMERA_SPEED;
            }
            break; 
        }   
    } else discard;
}
